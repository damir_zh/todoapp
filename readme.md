<a name="readme-top"></a>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#contacts">Contacts</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

This is my own project that I made for the portfolio and for testing various functions on the server.<br />
The project is deployed on the Ubuntu 22.04 server. Nginx, Gunicorn, Supervisor and etc.<br />
You can go to the project by following the link: https://35edition.kz

The project is in the editing stages and I will add new functionality as far as possible...

What has been done this moment:
1. Deployed on Ubuntu server
2. Added domain
3. Added SSL certificate by Certbot
4. Configured CI/CD for GitLab, updating project on server when repository updated
5. Send message for telegram when CI/CD ended
6. Configured Sentry for tracking exceptions and errors
7. Added HTML templates from bootstrapmade (but its doesnt work yet, work in progress, im not good in front)


What I want to add this project:
1. Simple functionality like ToDoApp, but add a lot of function like reminder, scores and etc.
2. Celery, Redis
3. WebSocket
4. Authorization from another platforms like Google
5. Authorization with QR code, JWT and etc.
6. And more and more...

<p align="right">(<a href="#readme-top">back to top</a>)</p>



### Built With

* [![Python][Python.com]][Python-url]
* [![Django][Django.com]][Django-url]
* [![Bootstrap][Bootstrap.com]][Bootstrap-url]

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

You can run this project folowing this steps

### Prerequisites

1. Install Python
  ```sh
  sudo apt install python
  ```

2. Install VirtualEnv
  ```sh
  pip install virtualenv
  ```

3. Install and create PostreSQL database

Maybe I didn't add everything needed...

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/damir_zh/todoapp.git
   ```
2. Create Virtual Environment
   ```sh
   virtualenv venv
   ```
3. Activate Virtual Environment
   ```sh
   source venv/bin/activate
   ```
4. Install packages
   ```sh
   pip install -r requirements.txt
   ```
5. Create migration files
   ```sh
   ./manage.py makemigrations
   ```
6. Migrate tables to database
   ```sh
   ./manage.py migrate
   ```
7. Migrate tables to database
   ```sh
   ./manage.py runserver
   ```


<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- CONTACT -->
## Contacts

My name is Damir Zhumabekov<br />
[![Telegram][Telegram.com]][Telegram-url]<br />
[![GitLab][Gitlab.com]][Gitlab-url]<br />
Email: damin_99.99@mail.ru, zhumabekov.damir1999@gmail.com

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- MARKDOWN LINKS & IMAGES -->
[Python.com]: https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54
[Python-url]: https://www.python.org

[Django.com]: https://img.shields.io/badge/django-%23092E20.svg?style=for-the-badge&logo=django&logoColor=white
[Django-url]: https://www.djangoproject.com

[Bootstrap.com]: https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white
[Bootstrap-url]: https://getbootstrap.com

[Telegram.com]: https://img.shields.io/badge/Telegram-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white
[Telegram-url]: https://t.me/Damir_Zh18

[Gitlab.com]: https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white
[Gitlab-url]: https://gitlab.com/damir_zh