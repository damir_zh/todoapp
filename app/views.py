from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from app.services import webhook_gitlab


class WebHookApi(APIView):

    def post(self, request, part: str):
        if part == 'gitlab':
            webhook_gitlab(request.data)
            return Response(status=status.HTTP_200_OK)

        return Response(status=status.HTTP_403_FORBIDDEN)
