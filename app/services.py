import requests

from config.settings import TELEGRAM_BOT_TOKEN


def webhook_gitlab(data: dict):
    ci_status = data['object_attributes']['status']
    if ci_status not in ['success', 'failed']:
        return

    project = data['project']['name']
    branch = data['object_attributes']['ref']
    duration_sec = data['object_attributes']['duration']
    author = data['user']['name']
    commit = data['commit']['title']
    commit_url = data['commit']['url']
    is_success = ci_status == 'success'
    icon = '✅' if is_success else '❌'

    __send_telegram_message(
        f'Branch: {branch} ({project})\n'
        f'Commit: <a href="{commit_url}">{commit}</a>\n'
        f'Author: {author}\n'
        f'Status: {icon} {duration_sec // 60}m {duration_sec % 60}s\n',
    )


def __send_telegram_message(text: str):
    url = f'https://api.telegram.org/bot{TELEGRAM_BOT_TOKEN}/sendMessage'
    requests.post(url, data={
        "chat_id": 319603251,  # For me
        "text": text,
        "parse_mode": "HTML",
        "disable_notification": False,
        "disable_web_page_preview": True
    })

