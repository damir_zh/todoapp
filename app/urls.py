from django.urls import path
from app.views import WebHookApi

urlpatterns = [
    path('webhook/<str:part>/', WebHookApi.as_view(), name='hooks'),
]
