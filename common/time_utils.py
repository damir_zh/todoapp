import pytz
from datetime import datetime

from django.utils.timezone import get_current_timezone


def server_tz() -> pytz.timezone:
    return get_current_timezone()


def to_server_tz(dt: datetime) -> datetime:
    return dt.astimezone(server_tz())


def server_now() -> datetime:
    return datetime.now(server_tz())
