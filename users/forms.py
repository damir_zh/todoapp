from django import forms

from users.models import User

class UserRegisterForm(forms.ModelForm):
    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
            'middle_name',
            'email',
            'phone_number'
        ]
