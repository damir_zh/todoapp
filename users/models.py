from datetime import datetime, timedelta

from imagekit.models import ProcessedImageField
from imagekit import processors

from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager

from users.exceptions import EmailExistsException, EmailIsEmptyException
from common.models import BaseModel
from common.time_utils import server_now
from config import settings


class UserManager(BaseUserManager):

    def create_user(self, **extra_fields):
        """
        Creates a new user, 'password' must be hashed (use the make_password method)
        """
        email = extra_fields.get('email')
        if not email:
            raise EmailIsEmptyException()

        email = email.strip().lower()
        extra_fields['email'] = email
        if User.objects.filter(email=email).exists():
            raise EmailExistsException()

        user = self.model.objects.create(**extra_fields)

        if not extra_fields.get('password'):
            password = self.model.objects.make_random_password()
            user.set_password(password)
            user.save(update_fields=['password'])
            user.send_mail_invitation(password=password)

        return user

    def create_superuser(self, email, password, **extra_fields):
        """
        Create and save a SuperUser with the given email and password.
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        user = self.model(email=email.lower(), **extra_fields)
        user.set_password(password)
        user.save()
        return user


class User(BaseModel, AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    middle_name = models.CharField(max_length=50, blank=True)
    email = models.EmailField(max_length=70, unique=True)
    phone_number = models.CharField(max_length=20, unique=True)
    avatar = ProcessedImageField(
        upload_to='avatar/',
        processors=[processors.Transpose()],
        format='WEBP',
        options={'quality': 60},
        null=True,
        blank=True
    )
    timezone = models.CharField(max_length=70, default=settings.TIME_ZONE)
    is_superuser = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    USERNAME_FIELD = 'email'

    @property
    def full_name(self):
        full_name = f'{self.last_name} {self.first_name}'
        if self.middle_name:
            full_name += f' {self.middle_name}'
        return full_name

    objects = UserManager()

    def __str__(self) -> str:
        return self.email


class OtpToken(BaseModel):
    token = models.CharField(max_length=32)
    code = models.CharField(max_length=8)
    phone_number = models.CharField(max_length=32)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    verified = models.BooleanField(default=False)
    action = models.CharField(max_length=50)

    @staticmethod
    def exp_date() -> datetime:
        return server_now() - timedelta(hours=1)
