import random

from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from django.utils.crypto import get_random_string

from users.models import OtpToken
from users.api.serializers import SendSmsSerializer
from users.services import clean_phone_number, send_sms
from users.exceptions import InvalidActionException, CustomException

class SendSmsAPIView(GenericAPIView):
    serializer_class = SendSmsSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data

        try:
            action = data['action']
            phone_number = clean_phone_number(data['phone_number'])
            if action == 'register':
                user = None
            else:
                raise InvalidActionException()
        except CustomException as e:
            return e.as_response()

        code = f'{random.randrange(10)}{random.randrange(10)}{random.randrange(10)}{random.randrange(10)}'
        text = f'Ваш код подтвеждения: {code}'
        send_sms(phone_number, text)

        otp_token = OtpToken.objects.create(
            user=user,
            phone_number=phone_number,
            code=code,
            token=get_random_string(length=32),
            action=action,
        )

        return Response({'message': 'success', 'otp_token': otp_token.token}, status=status.HTTP_200_OK)
