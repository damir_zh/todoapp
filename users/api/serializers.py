from rest_framework import serializers


class SendSmsSerializer(serializers.Serializer):
    action = serializers.ChoiceField(choices=['register'])
    phone_number = serializers.CharField()
