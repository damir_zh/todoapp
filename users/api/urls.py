from django.urls import path

from users.api import views


urlpatterns = [
    path('auth/sms/send/', views.SendSmsAPIView.as_view(), name='sms-send')
]