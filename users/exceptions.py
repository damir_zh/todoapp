from rest_framework import status
from rest_framework.response import Response
from rest_framework.exceptions import APIException

from django.utils.translation import gettext_lazy as _


class CustomException(APIException):
    default_code = status.HTTP_400_BAD_REQUEST
    default_detail = _('Неизвестная ошибка')
    default_code = 'error'

    def __init__(self, *args, **kwargs):
        super().__init__()
        self.status_code = kwargs.get('status_code') or self.status_code
        self.default_detail = kwargs.get('default_detail') or self.default_detail
        self.default_code = kwargs.get('default_code') or self.default_code

    def as_response(self) -> Response:
        return Response(
            {'message': self.default_detail, 'code': self.default_code},
            self.status_code,
        )


class EmailIsEmptyException(CustomException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = _('Пожалуйста, введите email')
    default_code = 'email-is-empty'


class EmailExistsException(CustomException):
    status_code = status.HTTP_423_LOCKED
    default_detail = _('Уже есть пользователь с такой почтой')
    default_code = 'email-exists'


class InvalidPhoneNumberException(CustomException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = _('Неправильный формат номера. Введите номер в формате +77071112233')
    default_code = 'invalid-phone-number'


class InvalidActionException(CustomException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = _('Неправильный action')
    default_code = 'invalid-action'
