from django.contrib import admin
from users.models import User

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ['id', 'full_name', 'email', 'is_active']
    list_display_links = ['id', 'email', 'full_name']
    list_filter = ['is_active', 'is_admin']
    search_fields = ['id', 'email', 'last_name', 'first_name', 'middle_name', 'phone_number']

    def full_name(self, obj):
        return obj.full_name
    
