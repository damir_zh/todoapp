from typing import Any
from django.db.models.query import QuerySet
from django.http import HttpRequest, HttpResponse
from django.shortcuts import redirect
from django.views.generic import TemplateView, CreateView

from users.forms import UserRegisterForm


from users.models import User

class IndexView(TemplateView):
    template_name = 'main/index.html'

    def get(self, request: HttpRequest, *args: Any, **kwargs: Any) -> HttpResponse:
        user = request.user
        if not user.is_authenticated:
            return redirect('login')
        return super().get(request, *args, **kwargs)


class LoginView(CreateView):
    template_name = 'users/login.html'
    queryset = User.objects.all()
    fields = ['first_name', 'last_name', 'middle_name']
    

    def post(self, request: HttpRequest, *args: str, **kwargs: Any) -> HttpResponse:
        return super().post(request, *args, **kwargs)


class RegisterView(CreateView):
    template_name = 'users/register.html'
    # queryset = User.objects.all()
    # fields = ['first_name', 'last_name', 'middle_name']
    form_class = UserRegisterForm

    def get_queryset(self) -> QuerySet[Any]:
        return self.queryset

    def post(self, request: HttpRequest, *args: str, **kwargs: Any) -> HttpResponse:
        return super().post(request, *args, **kwargs)
