import phonenumbers
import requests

from users.exceptions import InvalidPhoneNumberException
from config import settings


def clean_phone_number(phone_number: str) -> str:
    try:
        phone_number = phonenumbers.parse(phone_number)
    except phonenumbers.NumberParseException:
        raise InvalidPhoneNumberException()

    return f'+{phone_number.country_code}{phone_number.national_number}'


def send_sms(phone_number: str, text: str):
    if settings.IS_LOCAL_SERVER:
        print(f'SMS not sent, text: {text}')
        return

    data = {
        'recipient': phone_number.replace('+', ''),
        'text': text,
    }
    url = f'https://api.mobizon.kz/service/message/sendSmsMessage?output=json&api=v1&apiKey={settings.MOBIZON_API_KEY}'
    response = requests.request('POST', url, json=data)

    if response.status_code != 200:
        raise Exception(f'SMS not sent: {response.text}')
